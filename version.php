<?php

defined('MOODLE_INTERNAL') || die();

$plugin->version = 2014052000;
$plugin->requires   = 2013051400.00;
$plugin->release = 'v1.1.2 (Build: '.$plugin->version.')';
$plugin->maturity   = MATURITY_STABLE;

$plugin->dependencies = array(
    'local_adminer'             => ANY_VERSION,
    'block_progress'            => ANY_VERSION,
    'local_zilink'              => ANY_VERSION,
    'enrol_zilink'              => ANY_VERSION,
    'enrol_zilink_cohort'       => ANY_VERSION,
    'enrol_zilink_guardian'     => ANY_VERSION,
);